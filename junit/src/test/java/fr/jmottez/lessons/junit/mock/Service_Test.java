package fr.jmottez.lessons.junit.mock;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;

public class Service_Test {

	private Dao dao = mock(Dao.class);
	private Service service = new Service(dao);

	@Test
	public void testSave_IfDataValue() {
		Data data = new Data();
		data.setValue("abc");
		service.save(data);
		verify(dao).save(data);
	}

	@Test
	public void testNotSave_IfNotDataValue() {
		Data data = new Data();
		service.save(data);
		verify(dao, never()).save(data);
	}

	@Test
	public void testFIndByValue() {
		String value = "abc";
		Data data = new Data();
		data.setValue("123");
		when(dao.findByValue(value)).thenReturn(data);
		
		Data returnValue = service.findByValue(value);
		assertThat(returnValue.getValue(), is("123truc"));
	}

}
