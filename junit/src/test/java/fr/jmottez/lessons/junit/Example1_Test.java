package fr.jmottez.lessons.junit;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class Example1_Test {

	private Example1 exampleConcat = new Example1();
	private String value1 = "abc";
	private String value2 = "123";

	@Test
	public void concatTheFirstValueWithTheSecond() {
		assertThat(exampleConcat.concat(value1, value2), is("abc123"));
	}

	@Test
	public void returnEmptyWithNotFilledValues() {
		assertTrue(exampleConcat.concat("", "").isEmpty());
	}

	@Test
	public void returnStringObjet() {
		assertThat(exampleConcat.concat(value1, value2), instanceOf(String.class));
	}
}
