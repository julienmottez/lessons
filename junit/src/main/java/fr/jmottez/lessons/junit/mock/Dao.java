package fr.jmottez.lessons.junit.mock;

public interface Dao {

	void save(Data data);

	Data findByValue(String value);

}
