package fr.jmottez.lessons.designpattern.strategy.payment;


public class CreditCardStrategy implements PaymentStrategy {

    private String name;
    private String cardNumber;
    private String cvv;
    private String dateOfExpiry;

    public CreditCardStrategy(String name, String number, String cvv, String expiryDate) {
        this.name = name;
        this.cardNumber = number;
        this.cvv = cvv;
        this.dateOfExpiry = expiryDate;
    }

    public void pay(int amount) {
        System.out.println("Paiement par Carte de crédit de " + amount + ":" + name);
    }

}
