package fr.jmottez.lessons.designpattern.abs.factory.user.repository.jpa;

import fr.jmottez.lessons.designpattern.abs.factory.dao.jpa.JpaEntityBuilder;
import fr.jmottez.lessons.designpattern.abs.factory.user.model.UserModel;
import fr.jmottez.lessons.designpattern.abs.factory.user.repository.UserDao;
import fr.jmottez.lessons.designpattern.abs.factory.user.repository.jpa.assembler.UserJpaEntityAssembler;
import fr.jmottez.lessons.designpattern.abs.factory.user.repository.jpa.entity.UserJpaEntity;

import java.util.List;

public class JpaUserDao implements UserDao {

    private final JpaEntityBuilder entityBuilder;

    private UserJpaEntityAssembler assembler = new UserJpaEntityAssembler();

    public JpaUserDao(JpaEntityBuilder entityBuilder) {
        this.entityBuilder = entityBuilder;
    }

    public List<UserModel> findAll() {
        return assembler.fromEntities(entityBuilder.findAll(UserJpaEntity.class));
    }

    public List<UserModel> findByName(String name) {
        return assembler.fromEntities(entityBuilder.findBy(UserJpaEntity.class, "getUsrName", name));
    }
}
