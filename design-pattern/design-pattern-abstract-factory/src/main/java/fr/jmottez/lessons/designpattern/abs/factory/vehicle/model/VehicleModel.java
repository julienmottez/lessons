package fr.jmottez.lessons.designpattern.abs.factory.vehicle.model;


public class VehicleModel {

    private String registrationNumber;


    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }
}

