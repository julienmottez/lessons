package fr.jmottez.lessons.designpattern.abs.factory.user.model;


public class UserModel {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
