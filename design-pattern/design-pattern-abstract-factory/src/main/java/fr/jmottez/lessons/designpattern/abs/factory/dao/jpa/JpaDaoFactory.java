package fr.jmottez.lessons.designpattern.abs.factory.dao.jpa;


import fr.jmottez.lessons.designpattern.abs.factory.dao.DaoFactory;
import fr.jmottez.lessons.designpattern.abs.factory.user.repository.UserDao;
import fr.jmottez.lessons.designpattern.abs.factory.user.repository.jpa.JpaUserDao;
import fr.jmottez.lessons.designpattern.abs.factory.vehicle.repository.VehicleDao;
import fr.jmottez.lessons.designpattern.abs.factory.vehicle.repository.jpa.JpaVehicleDao;

public class JpaDaoFactory implements DaoFactory {

    private JpaEntityBuilder entityBuilder = new JpaEntityBuilder();

    public UserDao userDao() {
        return new JpaUserDao(entityBuilder);
    }

    public VehicleDao vehicleDao() {
        return new JpaVehicleDao(entityBuilder);
    }
}
