package fr.jmottez.lessons.designpattern.abs.factory.dao;


import fr.jmottez.lessons.designpattern.abs.factory.user.repository.UserDao;
import fr.jmottez.lessons.designpattern.abs.factory.vehicle.repository.VehicleDao;

public interface DaoFactory {

    UserDao userDao();

    VehicleDao vehicleDao();

}
