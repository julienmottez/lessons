package fr.jmottez.lessons.designpattern.abs.factory.user.repository;

import fr.jmottez.lessons.designpattern.abs.factory.user.model.UserModel;

import java.util.List;

public interface UserDao {

    List<UserModel> findAll();

    List<UserModel> findByName(String name);

}
