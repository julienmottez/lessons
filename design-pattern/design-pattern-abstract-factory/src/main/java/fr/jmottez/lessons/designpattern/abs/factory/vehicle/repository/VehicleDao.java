package fr.jmottez.lessons.designpattern.abs.factory.vehicle.repository;


import fr.jmottez.lessons.designpattern.abs.factory.vehicle.model.VehicleModel;

import java.util.List;

public interface VehicleDao {

    List<VehicleModel> findAll();

    List<VehicleModel> findByRegistrationNumber(String registrationNumber);

}
