package fr.jmottez.lessons.designpattern.abs.factory.dao.jpa;


import fr.jmottez.lessons.designpattern.abs.factory.user.repository.jpa.entity.UserJpaEntity;
import fr.jmottez.lessons.designpattern.abs.factory.vehicle.repository.jpa.entity.VehicleJpaEntity;
import com.google.common.collect.Lists;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

public class JpaEntityBuilder {

    private List<UserJpaEntity> users = Lists.newArrayList();
    private List<VehicleJpaEntity> vehicles = Lists.newArrayList();

    public JpaEntityBuilder() {
        UserJpaEntity user0 = new UserJpaEntity();
        user0.setUsrName("jpaUser0");
        users.add(user0);
        UserJpaEntity user1 = new UserJpaEntity();
        user1.setUsrName("jpaUser1");
        users.add(user1);

        VehicleJpaEntity vehicle0 = new VehicleJpaEntity();
        vehicle0.setVhlIdentify("jpaVhl0");
        vehicles.add(vehicle0);
        VehicleJpaEntity vehicle1 = new VehicleJpaEntity();
        vehicle1.setVhlIdentify("jpaVhl1");
        vehicles.add(vehicle1);
    }


    public <Entity> List<Entity> findAll(Class<Entity> clazz) {
        if (clazz.equals(UserJpaEntity.class)) {
            return (List<Entity>) users;
        }
        if (clazz.equals(VehicleJpaEntity.class)) {
            return (List<Entity>) vehicles;
        }
        return null;
    }

    public <Entity> List<Entity> findBy(Class<Entity> clazz, String parameterName, String parameterValue) {
        List<Entity> all = findAll(clazz);
        List<Entity> selected = Lists.newArrayList();
        Method method = null;
        try {
            method = clazz.getMethod(parameterName, new Class[]{});
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        if (method != null) {
            for (Entity each : all) {
                try {
                    if (parameterValue.equals(method.invoke(each))) {
                        selected.add(each);
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
        return selected;
    }
}
