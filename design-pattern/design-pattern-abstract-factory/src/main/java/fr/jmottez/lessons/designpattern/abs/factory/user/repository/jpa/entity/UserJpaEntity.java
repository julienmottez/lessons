package fr.jmottez.lessons.designpattern.abs.factory.user.repository.jpa.entity;


public class UserJpaEntity {

    private String usrName;

    public String getUsrName() {
        return usrName;
    }

    public void setUsrName(String usrName) {
        this.usrName = usrName;
    }
}
