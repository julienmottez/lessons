package fr.jmottez.lessons.designpattern.abs.factory.vehicle.repository.jdbc;

import fr.jmottez.lessons.designpattern.abs.factory.vehicle.model.VehicleModel;
import fr.jmottez.lessons.designpattern.abs.factory.vehicle.repository.VehicleDao;
import com.google.common.collect.Lists;

import java.util.List;

public class JdbcVehicleDao implements VehicleDao {

    private List<VehicleModel> vehicles = Lists.newArrayList();

    public JdbcVehicleDao() {
        VehicleModel vehicle0 = new VehicleModel();
        vehicle0.setRegistrationNumber("jdbcVehicle0");
        vehicles.add(vehicle0);
        VehicleModel vehicle1 = new VehicleModel();
        vehicle1.setRegistrationNumber("jdbcVehicle1");
        vehicles.add(vehicle1);
    }

    public List<VehicleModel> findAll() {
        return vehicles;
    }

    public List<VehicleModel> findByRegistrationNumber(String registrationNumber) {
        List<VehicleModel> selected = Lists.newArrayList();
        for (VehicleModel eachVehicle : vehicles) {
            if (eachVehicle.getRegistrationNumber().equals(registrationNumber)) {
                selected.add(eachVehicle);
            }
        }
        return selected;
    }
}
