package fr.jmottez.lessons.designpattern.abs.factory.dao.jdbc;


import fr.jmottez.lessons.designpattern.abs.factory.dao.DaoFactory;
import fr.jmottez.lessons.designpattern.abs.factory.user.repository.jdbc.JdbcUserDao;
import fr.jmottez.lessons.designpattern.abs.factory.user.repository.UserDao;
import fr.jmottez.lessons.designpattern.abs.factory.vehicle.repository.jdbc.JdbcVehicleDao;
import fr.jmottez.lessons.designpattern.abs.factory.vehicle.repository.VehicleDao;

public class JdbcDaoFactory implements DaoFactory {

    public UserDao userDao() {
        return new JdbcUserDao();
    }

    public VehicleDao vehicleDao() {
        return new JdbcVehicleDao();
    }
}
