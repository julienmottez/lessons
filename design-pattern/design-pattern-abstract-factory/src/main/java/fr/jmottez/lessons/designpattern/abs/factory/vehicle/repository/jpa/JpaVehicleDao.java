package fr.jmottez.lessons.designpattern.abs.factory.vehicle.repository.jpa;

import fr.jmottez.lessons.designpattern.abs.factory.dao.jpa.JpaEntityBuilder;
import fr.jmottez.lessons.designpattern.abs.factory.vehicle.model.VehicleModel;
import fr.jmottez.lessons.designpattern.abs.factory.vehicle.repository.VehicleDao;
import fr.jmottez.lessons.designpattern.abs.factory.vehicle.repository.jpa.assembler.VehicleJpaEntityAssembler;
import fr.jmottez.lessons.designpattern.abs.factory.vehicle.repository.jpa.entity.VehicleJpaEntity;


import java.util.List;

public class JpaVehicleDao implements VehicleDao {

    private final JpaEntityBuilder entityBuilder;

    private VehicleJpaEntityAssembler assembler = new VehicleJpaEntityAssembler();

    public JpaVehicleDao(JpaEntityBuilder entityBuilder) {
        this.entityBuilder = entityBuilder;
    }

    public List<VehicleModel> findAll() {
        return assembler.fromEntities(entityBuilder.findAll(VehicleJpaEntity.class));
    }

    public List<VehicleModel> findByRegistrationNumber(String registrationNumber) {
        return assembler.fromEntities(entityBuilder.findBy(VehicleJpaEntity.class, "getVhlIdentify", registrationNumber));
    }
}
