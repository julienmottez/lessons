package fr.jmottez.lessons.adapter;


public class UserToAdapte {

    private String fullName;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
