package fr.jmottez.lessons.adapter;


public class AdapterMain {


    public static void main(String[] args) {

        UserAdapted userAdapted = new UserAdapted();
        userAdapted.setFirstName("Paul");
        userAdapted.setLastName("Durant");

        UserToAdapte userToAdapte = new Adapter(userAdapted);
        System.out.println(userToAdapte.getFullName());

        userToAdapte.setFullName("Michel Dupont");
        System.out.println(userToAdapte.getFullName());


    }


}
