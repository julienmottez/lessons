package fr.jmottez.lessons.designpattern.lazy;


public class LazyLoadingObject {

    private String value;


    public String getValue() {
        if(value == null){
            value = loadingValue();
        }
        return value;
    }

    private String loadingValue() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "anyValue";
    }

    public String getKey() {
        return "anyKey";
    }
}
