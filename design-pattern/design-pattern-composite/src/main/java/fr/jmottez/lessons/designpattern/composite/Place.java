package fr.jmottez.lessons.designpattern.composite;

import com.google.common.collect.Lists;

import java.util.List;

public class Place {

    private String description;
    private Point x;
    private Point y;
    private List<Place> children = Lists.newArrayList();

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setX(double x, double y) {
        this.x = new Point(x, y);
    }

    public Point getX() {
        return x;
    }

    public void setY(double x, double y) {
        this.y = new Point(x, y);
    }

    public Point getY() {
        return y;
    }

    public List<Place> getChildren() {
        return children;
    }

    @Override
    public String toString() {
        return "Place{" +
                "description='" + description + '\'' +
                ", x=" + x +
                ", y=" + y +
                '}';
    }
}
