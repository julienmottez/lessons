package fr.jmottez.lessons.designpattern.composite;


public class CompositeMain {


    public static void main(String[] args) {

        Place france = new Place();
        france.setDescription("France");
        france.setX(50.0, 7.2);
        france.setY(55.0, 13.5);

        Place bourgogne = new Place();
        bourgogne.setDescription("Cote d'or");
        bourgogne.setX(52.0, 8.2);
        bourgogne.setY(53.0, 8.3);
        france.getChildren().add(bourgogne);


        Place boucheDuRhone = new Place();
        boucheDuRhone.setDescription("Bouche du Rhone");
        boucheDuRhone.setX(54.0, 8.2);
        boucheDuRhone.setY(55.0, 8.4);
        france.getChildren().add(boucheDuRhone);


        Place aixEnProvence = new Place();
        aixEnProvence.setDescription("Aix en Provence");
        aixEnProvence.setX(54.55, 8.2);
        aixEnProvence.setY(54.56, 8.21);
        boucheDuRhone.getChildren().add(aixEnProvence);

        Place marseille = new Place();
        marseille.setDescription("Marseille");
        marseille.setX(54.58, 8.3);
        marseille.setY(54.59, 8.32);
        boucheDuRhone.getChildren().add(marseille);

        display(france);
    }

    private static void display(Place place) {
        System.out.println(place.toString());
        for(Place child : place.getChildren()){
            display(child);
        }
    }


}
