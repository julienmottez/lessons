package fr.jmottez.lessons.designpattern.factory.model.lemming;



public interface Lemming {

    void action();

}
