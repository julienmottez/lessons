package fr.jmottez.lessons.designpattern.factory.model.lemming;


public enum LemmingType {

    CLIMBER, BOMBER, DIGGER;

}
