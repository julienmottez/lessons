package fr.jmottez.lessons.designpattern.factory.model.lemming;


public class ClimberLemming implements Lemming {

    public void action() {
        System.out.println("Climb !!");
    }

}
