package fr.jmottez.lessons.designpattern.factory.model.lemming;


public class BomberLemming implements Lemming {

    public void action() {
        System.out.println("Bomb !!");
    }

}