package fr.jmottez.lessons.designpattern.proxy;


public class ProxyMain {


    public static void main(String[] args) {
        Image image = new ProxyImage("toto.jpg");
        image.display();
    }


}
