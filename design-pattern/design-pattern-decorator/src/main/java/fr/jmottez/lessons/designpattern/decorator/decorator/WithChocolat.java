package fr.jmottez.lessons.designpattern.decorator.decorator;


import fr.jmottez.lessons.designpattern.decorator.model.Dessert;

public class WithChocolat extends DessertDecorator {

    public WithChocolat(Dessert dessert) {
        super(dessert);
    }

    public String getLabel() {
        return getDessert().getLabel() + ", chocolat";
    }

    public double getPrice() {
        return getDessert().getPrice() + 0.20;
    }
}
