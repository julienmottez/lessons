package fr.jmottez.lessons.designpattern.decorator.decorator;


import fr.jmottez.lessons.designpattern.decorator.model.Dessert;

public abstract class DessertDecorator extends Dessert {

    private Dessert dessert;

    public DessertDecorator(Dessert dessert) {
        this.dessert = dessert;
    }

    public abstract String getLabel();

    public abstract double getPrice();


    protected Dessert getDessert() {
        return dessert;
    }
}
