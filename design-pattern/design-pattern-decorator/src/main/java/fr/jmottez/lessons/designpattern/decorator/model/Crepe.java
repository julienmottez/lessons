package fr.jmottez.lessons.designpattern.decorator.model;


public class Crepe extends Dessert {
    public Crepe() {
        setLabel("Crêpe");
        setPrice(1.20);
    }
}