package fr.jmottez.lessons.designpattern.prototype;


public class Vehicle implements Cloneable {

    private String ref;

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getRef() {
        return ref;
    }


    @Override
    public Vehicle clone() throws CloneNotSupportedException {
        Vehicle vehicleCopy = new Vehicle();
        vehicleCopy.setRef(getRef() + "_cloned");
        return vehicleCopy;
    }

}

