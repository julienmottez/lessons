package fr.jmottez.lessons.designpattern.prototype;

import com.google.common.collect.Lists;

import java.util.List;

public class User implements Cloneable {

    private String name;

    private List<Vehicle> vehicles;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Vehicle> getVehicles() {
        if (vehicles == null) {
            vehicles = Lists.newArrayList();
        }
        return vehicles;
    }

    @Override
    public User clone() throws CloneNotSupportedException {
        User userCopy = new User();
        userCopy.setName(getName() + "_cloned");
        for (Vehicle vehicle : getVehicles()) {
            userCopy.getVehicles().add(vehicle.clone());
        }
        return userCopy;
    }
}
