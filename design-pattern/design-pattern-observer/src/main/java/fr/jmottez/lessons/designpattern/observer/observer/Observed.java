package fr.jmottez.lessons.designpattern.observer.observer;


import fr.jmottez.lessons.designpattern.observer.event.Event;
import com.google.common.collect.Lists;

import java.util.List;

public class Observed {

    private List<Observer> observers = Lists.newArrayList();


    public void add(Observer observer) {
        observers.add(observer);
    }

    public void delete(Observer observer) {
        observers.remove(observer);
    }

    protected void notify(Event event) {
        for (Observer observer : observers) {
            observer.onEvent(event);
        }
    }
}
