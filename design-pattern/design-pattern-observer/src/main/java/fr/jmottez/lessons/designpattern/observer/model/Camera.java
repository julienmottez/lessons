package fr.jmottez.lessons.designpattern.observer.model;


import fr.jmottez.lessons.designpattern.observer.event.WarningEvent;
import fr.jmottez.lessons.designpattern.observer.observer.Observed;

public class Camera extends Observed {

    public void seeSomeOne() {
        notify(new WarningEvent());
    }
}
