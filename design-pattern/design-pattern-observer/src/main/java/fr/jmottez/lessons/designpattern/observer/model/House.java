package fr.jmottez.lessons.designpattern.observer.model;


import fr.jmottez.lessons.designpattern.observer.event.AlertEvent;
import fr.jmottez.lessons.designpattern.observer.event.Event;
import fr.jmottez.lessons.designpattern.observer.event.WarningEvent;
import fr.jmottez.lessons.designpattern.observer.observer.Observer;

public class House implements Observer {

    public void onEvent(Event event) {
        if(event instanceof WarningEvent){
            System.out.println("send SMS to the owner");
        } else if(event instanceof AlertEvent){
            System.out.println("ring Alarm !!");
        }
    }
}
