package fr.jmottez.lessons.assembler;


public class AssemblerMain {

    private static User1Assembler assembler = new User1Assembler();

    public static void main(String[] args) {

        User1 user1 = new User1();
        user1.setFirstName("first1");
        user1.setLastName("last1");

        User2 user2 = assembler.toUser2(user1);
        System.out.println(user2.getFullName());
        User2 otherUser2 = assembler.toUser2(user1);
        System.out.println(otherUser2.getFullName());

        User1 otherUser1 = assembler.fromUser2(otherUser2);
        System.out.println(otherUser1.getFirstName());
        System.out.println(otherUser1.getLastName());

    }


}
