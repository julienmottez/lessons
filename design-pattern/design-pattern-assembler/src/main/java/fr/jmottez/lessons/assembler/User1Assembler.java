package fr.jmottez.lessons.assembler;


public class User1Assembler {

    public User2 toUser2(User1 user1) {
        User2 user2 = new User2();
        user2.setFullName(user1.getFirstName() + " " + user1.getLastName());
        return user2;
    }

    public User1 fromUser2(User2 user2) {
        String[] splitfullName = user2.getFullName().split(" ");
        User1 user1 = new User1();
        user1.setFirstName(splitfullName[0]);
        user1.setLastName(splitfullName[1]);
        return user1;
    }
}
