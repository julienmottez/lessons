package fr.jmottez.lessons.builder.model;

import fr.jmottez.lessons.builder.model.burger.Burger;
import fr.jmottez.lessons.builder.model.dessert.Dessert;
import com.google.common.collect.Lists;

import java.util.List;

public class FastFoodMenu {

    private MenuType type;
    private Burger burger;
    private FriesType friesType;
    private List<Dessert> desserts;
    private double price;

    public void setType(MenuType type) {
        this.type = type;
    }

    public MenuType getType() {
        return type;
    }

    public void setBurger(Burger burger) {
        this.burger = burger;
    }

    public Burger getBurger() {
        return burger;
    }

    public void setFriesType(FriesType friesType) {
        this.friesType = friesType;
    }

    public FriesType getFriesType() {
        return friesType;
    }


    public List<Dessert> getDesserts() {
        if (desserts == null) {
            desserts = Lists.newArrayList();
        }
        return desserts;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        String burgerName = "";
        if (burger != null) {
            burgerName = burger.getClass().getSimpleName();
        }
        String dessertsName = "";
        for (Dessert dessert : getDesserts()) {
            if (!dessertsName.isEmpty()) {
                dessertsName += ", ";
            }
            dessertsName += dessert.getClass().getSimpleName();
        }

        return "FastFoodMenu{" +
                "type=" + type +
                ", burger=" + burgerName +
                ", friesType=" + friesType +
                ", desserts=" + dessertsName +
                ", price=" + price + "€" +
                '}';
    }


}
