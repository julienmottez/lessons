package fr.jmottez.lessons.builder;

import com.google.common.collect.Lists;
import fr.jmottez.lessons.builder.exception.MenuBuilderException;
import fr.jmottez.lessons.builder.model.FastFoodMenu;
import fr.jmottez.lessons.builder.model.FriesType;
import fr.jmottez.lessons.builder.model.MenuType;
import fr.jmottez.lessons.builder.model.burger.Burger;
import fr.jmottez.lessons.builder.model.dessert.Dessert;

import java.util.List;

public class FastFoodMenuBuilder {

    private FastFoodMenuPriceCalculator priceCalculator = new FastFoodMenuPriceCalculator();

    private MenuType menuType;
    private Burger burger;
    private FriesType friesType;
    private List<Dessert> desserts = Lists.newArrayList();

    public FastFoodMenuBuilder() {
        clean();
    }

    public FastFoodMenuBuilder clean() {
        menuType = MenuType.EMPTY;
        friesType = FriesType.EMPTY;
        burger = null;
        desserts.clear();
        return this;
    }

    public FastFoodMenuBuilder setMenuType(MenuType menuType) {
        this.menuType = menuType;
        return this;
    }

    public FastFoodMenuBuilder setBurger(Burger burger) {
        this.burger = burger;
        return this;
    }

    public FastFoodMenuBuilder setFriesType(FriesType friesType) {
        this.friesType = friesType;
        return this;
    }

    public FastFoodMenuBuilder addDessert(Dessert dessert) {
        desserts.add(dessert);
        return this;
    }

    public FastFoodMenu build() throws MenuBuilderException {
        checkValid();
        return buildAfterCheckValid();
    }

    private void checkValid() throws MenuBuilderException {
        if (menuType == null || menuType.equals(MenuType.EMPTY)) {
            throw new MenuBuilderException("Le menu n'est pas sélectionné.");
        }
        if (burger == null) {
            throw new MenuBuilderException("Le burger doit être saisi.");
        }
        if (friesType == null || friesType.equals(FriesType.EMPTY)) {
            throw new MenuBuilderException("Le type de frite doit être saisi.");
        }
    }

    private FastFoodMenu buildAfterCheckValid() {
        FastFoodMenu menu = new FastFoodMenu();
        menu.setType(menuType);
        menu.setBurger(burger);
        menu.setFriesType(friesType);
        menu.getDesserts().addAll(desserts);
        menu.setPrice(priceCalculator.calculate(menu));
        return menu;
    }


}
