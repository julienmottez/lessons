package fr.jmottez.lessons.designpattern.singleton;

import java.util.Locale;

public class AppSetting {

    private static AppSetting instance = new AppSetting();

    public static AppSetting getInstance() {
        return instance;
    }

    private AppSetting() {
    }

    private User userSession;

    private String dateFormat;

    private Locale locale;


    public User getUserSession() {
        return userSession;
    }

    public void setUserSession(User userSession) {
        this.userSession = userSession;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }
}

