package fr.jmottez.lessons.designpattern.singleton;


public class FirstLayer {

    private SecondLayer secondLayer = new SecondLayer();

    public String date() {
        return secondLayer.date();
    }

    public String request() {
        return secondLayer.request();
    }
}
