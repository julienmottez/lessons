package fr.jmottez.lessons.designpattern.singleton;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SecondLayer {

    private ThirdLayer thirdLayer = new ThirdLayer();

    public String date() {
        DateFormat dateFormat = new SimpleDateFormat(AppSetting.getInstance().getDateFormat());
        return dateFormat.format(new Date());
    }

    public String request() {
        return "SELECT * FROM User user where user.id = " + AppSetting.getInstance().getUserSession().getId();
    }
}
