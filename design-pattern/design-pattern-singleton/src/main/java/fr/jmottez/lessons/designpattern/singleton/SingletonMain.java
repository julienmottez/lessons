package fr.jmottez.lessons.designpattern.singleton;


import java.util.Locale;

public class SingletonMain {


    private static FirstLayer firstLayer = new FirstLayer();

    public static void main(String[] args) {

        User userConnected = new User();
        userConnected.setId(12);
        AppSetting.getInstance().setUserSession(userConnected);
        AppSetting.getInstance().setDateFormat("dd-MM-yyyy");
        AppSetting.getInstance().setLocale(Locale.FRANCE);

        System.out.println(firstLayer.date());
        System.out.println(firstLayer.request());

    }


}
