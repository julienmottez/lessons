package fr.jmottez.lessons.designpattern.singleton;


public class User {

    private int id;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
