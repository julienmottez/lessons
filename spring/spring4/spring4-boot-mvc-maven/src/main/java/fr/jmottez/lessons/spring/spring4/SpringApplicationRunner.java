package fr.jmottez.lessons.spring.spring4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringApplicationRunner {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(SpringApplicationRunner.class, args);
	}

}
